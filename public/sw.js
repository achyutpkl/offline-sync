self.importScripts('idb.js');
self.importScripts('idbStore.js');

self.addEventListener('sync', function(event) {
  event.waitUntil(
    idbStore
      .entry('readonly')
      .then(function(entry) {
        return entry.getAll();
      })
      .then(function(dataEntries) {
        return Promise.all(
          dataEntries.map(function(dataEntry) {
            const postData = {
              fullName: dataEntry.fullName,
              gender: dataEntry.gender,
              occupation: dataEntry.occupation
            };

            return fetch(dataEntry.url, {
              method: 'POST',
              body: JSON.stringify(postData),
              headers: {
                Accept: 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/json'
              }
            })
              .then(function(response) {
                return response.json();
              })
              .then(function(data) {
                if (data.fullName === dataEntry.fullName) {
                  return idbStore.entry('readwrite').then(function(entry) {
                    return entry
                      .delete(dataEntry.id)
                      .then(function(deleteResult) {
                        self.clients.matchAll().then(function(clients) {
                          clients.forEach(function(client) {
                            client.postMessage({
                              msg: 'An item in indexedDB was deleted',
                              type: 'idbUpdate'
                            });
                          });
                        });
                      })
                      .catch(function(error) {
                        console.error(error);
                      });
                  });
                }
              });
          })
        );
      })
      .catch(function(err) {
        console.error(err);
      })
  );
});
