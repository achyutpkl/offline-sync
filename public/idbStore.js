var idbStore = {
  db: null,

  init: function() {
    if (idbStore.db) {
      return Promise.resolve(idbStore.db);
    }

    return idb
      .open('dataEntries', 1, function(upgradeDb) {
        upgradeDb.createObjectStore('entry', {
          autoIncrement: true,
          keyPath: 'id'
        });
      })
      .then(function(db) {
        return (idbStore.db = db);
      });
  },

  entry: function(mode) {
    return idbStore.init().then(function(db) {
      return db.transaction('entry', mode).objectStore('entry');
    });
  }
};
