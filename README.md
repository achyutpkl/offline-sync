# Online / Offline Form

## How to run?

`yarn`

**For dev:**

`yarn start`

**For Production**

`yarn build`
`yarn electron`

## What to expect?

- If you have internet access, then it will directly send request to server
- Try turning off the wifi connection, you should see the message log, _connection lost_
- In this case, if you submit the form, then it will be stored in local database
- Once there is connection available (Turn on wifi now), app will sync offline stored data to server
