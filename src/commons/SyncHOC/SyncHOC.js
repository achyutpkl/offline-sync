import React, { PureComponent } from "react";

import {
  sync,
  sendRequest,
  getOfflineDataFromLocalStorage
} from "../../services/sync";

import SyncMessage from "../../views/SyncMessage";

let syncListener = WrappedComponent => {
  return class extends PureComponent {
    constructor() {
      super();

      this.state = {
        isOnline: true,
        isSyncing: false,
        isSubmitting: false,
        numUnsyncedData: getOfflineDataFromLocalStorage().length
      };

      this.syncData = this.syncData.bind(this);
      this.sendDataAndUpdateCount = this.sendDataAndUpdateCount.bind(this);
    }

    syncData() {
      this.setState({ isSyncing: true });

      sync()
        .then(isUpdateCount => {
          this.setState({ isSyncing: false, isOnline: true });

          if (!isUpdateCount) {
            return;
          }

          this.updateCount();
        })
        .catch(error => {
          this.setState({ isSyncing: false });
        });
    }

    componentDidMount() {
      window.addEventListener("online", this.syncData);
      window.addEventListener("offline", this.setOffline);

      this.syncData();
    }

    setOffline = () => {
      console.log("Internet connection lost");
      this.setState({ isOnline: false });
    };

    updateCount() {
      this.setState({
        numUnsyncedData: getOfflineDataFromLocalStorage().length
      });
    }

    sendDataAndUpdateCount(data) {
      this.setState({ isSubmitting: true });

      sendRequest(data)
        .then(isUpdateCount => {
          this.setState({ isSubmitting: false });

          if (!isUpdateCount) {
            return;
          }

          this.updateCount();
        })
        .catch(error => {
          this.setState({ isSubmitting: false });
        });
    }

    render() {
      return (
        <div
          style={{ backgroundColor: this.state.isOnline ? "" : "#80808030" }}
        >
          <SyncMessage
            isSyncing={this.state.isSyncing}
            isSubmitting={this.state.isSubmitting}
            numEntries={this.state.numUnsyncedData}
          />
          <WrappedComponent
            sendRequest={this.sendDataAndUpdateCount}
            {...this.props}
          />
        </div>
      );
    }
  };
};

export default syncListener;
