import React, { Component } from "react";

import "./App.css";

import { addCallbackToConsole } from "./services/console";

import Form from "./views/Form";
import Topbar from "./views/Topbar";
import MessageLogs from "./views/MessageLogs";

class App extends Component {
  constructor() {
    super();

    this.state = {
      messages: []
    };

    this.appendLogMessage = this.appendLogMessage.bind(this);
  }

  componentWillMount() {
    addCallbackToConsole(this.appendLogMessage);
  }

  appendLogMessage(message) {
    if (!message) {
      return;
    }

    const messages = [...this.state.messages];

    messages.unshift(message);

    this.setState({ messages });
  }

  render() {
    return (
      <div className="app-wrapper">
        <Topbar />
        <Form />
        <hr />
        <div className="title">Message Logs</div>
        <MessageLogs messages={this.state.messages} />
      </div>
    );
  }
}

export default App;
