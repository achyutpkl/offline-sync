export default function registerMyServiceWorker() {
  if ("serviceWorker" in navigator) {
    window.addEventListener("load", () => {
      const idbStore = window.idbStore;

      function getEntriesInDb() {
        return idbStore.entry("readonly").then(entry => entry.getAll());
      }

      const form = document.querySelector("#entry-form");
      const fullNameField = form.querySelector("#fullName");
      const genderField = form.querySelector("#gender");
      const occupationField = form.querySelector("#occupation");

      const syncMessageElement = document.querySelector("#sync-status-message");

      function getSyncStatusMessage(entries) {
        if (entries && entries.length) {
          if (entries.length === 1) {
            return "1 entry needs to be synced.";
          }

          return entries.length + " entries need to be synced.";
        }

        return "All items are synced and updated.";
      }

      function clearForm() {
        fullNameField.value = "";
        genderField.value = "";
        occupationField.value = "";
      }

      function setSyncMessage(message) {
        syncMessageElement.innerHTML = message;
      }

      function updateSyncMessage() {
        console.log("updating sync message...");
        getEntriesInDb().then(dataEntries => {
          setSyncMessage(getSyncStatusMessage(dataEntries));
        });
      }

      navigator.serviceWorker.register("sw.js").then(
        registration => {
          if ("sync" in registration) {
            form.addEventListener("submit", event => {
              event.preventDefault();

              const dataEntry = {
                fullName: fullNameField.value,
                gender: genderField.value,
                occupation: occupationField.value,
                url: form.action
              };

              idbStore
                .entry("readwrite")
                .then(outbox => {
                  return outbox.put(dataEntry);
                })
                .then(() => {
                  clearForm();

                  updateSyncMessage();

                  return registration.sync.register("entry");
                })
                .catch(err => {
                  console.error(err);

                  form.submit();
                });
            });

            navigator.serviceWorker.addEventListener("message", event => {
              console.log("event message.........");
              if (event.data.type === "idbUpdate") {
                updateSyncMessage();
              }
            });
          } else {
            setSyncMessage("Your device doesn't support background sync.");
          }
        },
        err => {
          console.error("ServiceWorker registration failed: ", err);
        }
      );
    });
  }
}
