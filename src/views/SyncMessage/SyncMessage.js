import React from "react";

import LinearProgress from "@material-ui/core/LinearProgress";
import CircularProgress from "@material-ui/core/CircularProgress";

const getSyncStatusMessage = (isSyncing, isSubmitting, numEntries) => {
  if (isSyncing) {
    return "Syncing data...";
  }

  if (isSubmitting) {
    return "Submitting data...";
  }

  if (numEntries) {
    if (numEntries === 1) {
      return "1 entry needs to be synced.";
    }

    return numEntries + " entries need to be synced.";
  }

  return "No unsynced data.";
};

const SyncMessage = props => {
  const { isSyncing, isSubmitting, numEntries } = props;

  return (
    <div id="sync-status-message-wrapper">
      <div style={{ width: "100%", padding: "0px 24px" }}>
        {!isSyncing && isSubmitting ? <LinearProgress /> : ""}
      </div>
      <div id="sync-status-message">
        <div className="status-spinner-wrapper">
          {isSyncing ? <CircularProgress size={40} /> : ""}
        </div>
        {getSyncStatusMessage(isSyncing, isSubmitting, numEntries)}
      </div>
    </div>
  );
};

export default SyncMessage;
