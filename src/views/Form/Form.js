import React, { PureComponent } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import syncListener from "../../commons/SyncHOC";

class Form extends PureComponent {
  state = {
    name: "",
    gender: "",
    occupation: ""
  };

  render() {
    return (
      <div>
        <form id="entry-form" onSubmit={this._onSubmit}>
          <TextField
            required
            value={this.state.name}
            id="fullName"
            className="data-input-field"
            label="Full Name"
            name="name"
            margin="normal"
            placeholder="Full Name"
            onChange={e => this._onChange(e)}
          />
          <TextField
            required
            value={this.state.gender}
            id="gender"
            className="data-input-field"
            label="Gender"
            name="gender"
            margin="normal"
            placeholder="Gender"
            onChange={e => this._onChange(e)}
          />
          <TextField
            id="occupation"
            value={this.state.occupation}
            className="data-input-field"
            label="Occupation"
            name="occupation"
            margin="normal"
            placeholder="Occupation"
            onChange={e => this._onChange(e)}
          />
          <div className="button-wrapper">
            <Button
              variant="contained"
              color="primary"
              // onClick={this._onSubmit}
              type="submit"
            >
              Submit
            </Button>
          </div>
        </form>
      </div>
    );
  }

  _onSubmit = e => {
    console.log("aayo aao");
    e.preventDefault();

    let data = {
      id: String(Math.random()),
      method: "POST",
      url: "https://jsonplaceholder.typicode.com/posts",
      payload: this.state
    };

    this.props.sendRequest(data);
    this.setState({ name: "", gender: "", occupation: "" });
  };

  _onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
}

export default syncListener(Form);
