import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

// NOTE: Never call console statements inside this component. It will cause an infinite loop.
const MessageLogs = props => {
  const { messages } = props;

  return (
    <List dense={true}>
      {messages.map(function(message, i) {
        return (
          <ListItem key={i}>
            <ListItemText primary={`${messages.length - i}.  ${message}`} />
          </ListItem>
        );
      })}
    </List>
  );
};

export default MessageLogs;
