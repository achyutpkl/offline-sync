import React from "react";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const Topbar = () => {
  return (
    <AppBar position="static" color="primary" title="Online / Offline Form">
      <Toolbar>
        <Typography variant="title" color="inherit">
          Online / Offline Form
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Topbar;
