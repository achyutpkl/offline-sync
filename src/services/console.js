export function addCallbackToConsole(callback) {
  if (!callback) {
    return;
  }

  if (typeof console === 'undefined') {
    console = {};
  }

  if (typeof console.log !== 'undefined') {
    console.olog = console.log;
  } else {
    console.olog = function() {};
  }

  console.log = function(message, variable) {
    if (variable) {
      console.olog(message, variable);
      callback(message + JSON.stringify(variable));

      return;
    }

    console.olog(message);
    callback(message);
  };

  console.error = console.debug = console.info = console.log;

  return;
}
