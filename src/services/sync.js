/*
  In localstorage, save the data with format like:

  offlineData: [{
    url: ''                     // server url where you need to send request
    method: 'post, get'         // request method
    payload: {}                    // data / payload
  }]

*/

const LOCALSTORAGE_KEY = "offlineData";

/**
 * This method sync the existing data on cache. It is called from syncHOC component.
 */
const sync = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (!navigator.onLine) {
        console.log("offline so aborting sync");

        return resolve(false);
      }

      console.log("Internet connected, syncing cache data to server...");
      let offlineDataUpdated = false;

      // resolve all promises representing possible api calls
      return Promise.all(
        getOfflineDataFromLocalStorage().map(data => {
          console.log("sending cached data to: ", data.url); // after this, remove from localstorage

          return Promise.resolve(true).then(isSuccess => {
            if (!isSuccess) {
              return false;
            }

            removeOfflineDataInLocalStorage(data.id);

            offlineDataUpdated = true;

            return true;
          });
        })
      )
        .then(() => {
          if (offlineDataUpdated) {
            return resolve(true);
          }

          return resolve(false);
        })
        .catch(error => {
          console.error(error);

          return resolve(true);
        });
    }, 2000);
  });
};

/**
 *
 * @param {Object} data => {url:'', method:'', payload: ''}
 */
const sendRequest = data => {
  return new Promise((resolve, reject) => {
    if (!isValidPayload(data)) {
      return resolve(false);
    }

    // if online, send request to server, else store to local storage
    if (navigator.onLine) {
      console.log("Has access to internet so, sending to server.....");
      setTimeout(() => {
        return Promise.resolve(true).then(isSuccess => {
          if (!isSuccess) {
            console.error("The request was unsuccessful.");
          }

          return resolve(false);
        });
      }, 2500);
    } else {
      console.log("No internet so, storing in local storage");
      saveOfflineDataInLocalStorage(data);

      return resolve(true);
    }
  });
};

const saveOfflineDataInLocalStorage = data => {
  if (!isValidPayload(data)) {
    return;
  }

  let offlineData = getOfflineDataFromLocalStorage();
  offlineData.push(data);
  localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(offlineData));
};

const removeOfflineDataInLocalStorage = dataId => {
  if (!dataId) {
    console.error("Attempted to remove data with an invalid id");

    return;
  }

  let offlineData = getOfflineDataFromLocalStorage();

  offlineData = offlineData.filter(data => data.id !== dataId);

  localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(offlineData));
};

const getOfflineDataFromLocalStorage = () => {
  let offlineData = localStorage.getItem(LOCALSTORAGE_KEY);

  if (!offlineData) {
    console.log("empty local storage");
    return [];
  }

  return JSON.parse(offlineData);
};

const isValidPayload = data => {
  if (!(data.url && data.method && data.id)) {
    console.log("invalid payload, missing either id, url or method");
    return false;
  }

  return true;
};

export { sync, sendRequest, getOfflineDataFromLocalStorage };
